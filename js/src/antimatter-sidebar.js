/**
 * Created by Kupletsky Sergey on 17.10.14.
 *
 * Material Sidebar (Profile menu)
 * Tested on Win8.1 with browsers: Chrome 37, Firefox 32, Opera 25, IE 11, Safari 5.1.7
 * You can use this sidebar in Bootstrap (v3) projects. HTML-markup like Navbar bootstrap component will make your work easier.
 * Dropdown menu and sidebar toggle button works with JQuery and Bootstrap.min.js
 */

/*
*Animatter Wrapper for Material Sidebar @ based  on http://codepen.io/zavoloklom/pen/dIgco
*/

// Sidebar toggle
//
// -------------------

var antimatter = antimatter || {};


//Test json data
var json = {
  coverImage: "http://2.bp.blogspot.com/-2RewSLZUzRg/U-9o6SD4M6I/AAAAAAAADIE/voax99AbRx0/s1600/14%2B-%2B1%2B%281%29.jpg",
  brandImage: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/53474/atom_profile_01.jpg",
  coverTitle: "AntiMatter",
  coverDropdown: [
    {title: "Yehuda1", href: "#Katz", tabIndex: "1"},
    {title: "Yehuda2", href: "#Katz", tabIndex: "1"}, 
    {title: "Yehuda3", href: "#Katz", tabIndex: "1"},     
  ],
  firstMenuSection: [
    {title: "Yehuda1", href: "#Katz", icon: "md-inbox"},
    {title: "Yehuda2", href: "#Katz", icon: "md-star"}, 
    {title: "Yehuda3", href: "#Katz", icon: "md-send"},
    {title: "Yehuda3", href: "#Katz", icon: "md-drafts"},     
  ] 
};

SideBar.prototype = new Antimatter();
SideBar.prototype.constructor = SideBar;
function SideBar(htmlId, name) {
    this.constructor.instanceActive++;
    this.constructor.POS_DEFAULT = '';
    this.constructor.POS_FLOAT_LEFT = 'sidebar-fixed-left';
    this.constructor.POS_FLOAT_RIGHT = 'sidebar-fixed-right';    
    this.constructor.POS_STACK_LEFT = 'sidebar-stacked';
    this.constructor.DEFUALT_IMAGE;   
    this.htmlId = function(){ return htmlId }
    this.name = function(){ return name }
    this.uuid = this.uniqueID();
    this.selector = null;    
}

SideBar.instanceActive = 0;

SideBar.prototype.eventWireUp = function() {
    var overlay = $('.sidebar-overlay');
    var toggleButtons = $('.sidebar-toggle');

    $('.sidebar-toggle').on('click', function() {
        var sidebar = $('#sidebar');
        sidebar.toggleClass('open');
        if ((sidebar.hasClass('sidebar-fixed-left') || sidebar.hasClass('sidebar-fixed-right')) && sidebar.hasClass('open')) {
            overlay.addClass('active');
        } else {
            overlay.removeClass('active');
        }
    });

    overlay.on('click', function() {
        $(this).removeClass('active');
        $('#sidebar').removeClass('open');
    });    

    SideBar.DEFUALT_IMAGE = $('#sidebar .sidebar-header').css('background-image');

    // Hide toggle buttons on default position
    toggleButtons.css('display', 'none');
    $('body').css('display', 'table');    
}

SideBar.prototype.render = function () {

    (function($) {
        var dropdown = $('.dropdown');

        // Add slidedown animation to dropdown
        dropdown.on('show.bs.dropdown', function(e){
            $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
        });

        // Add slideup animation to dropdown
        dropdown.on('hide.bs.dropdown', function(e){
            $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
        });
    })(jQuery);



    (function(removeClass) {

        jQuery.fn.removeClass = function( value ) {
            if ( value && typeof value.test === "function" ) {
                for ( var i = 0, l = this.length; i < l; i++ ) {
                    var elem = this[i];
                    if ( elem.nodeType === 1 && elem.className ) {
                        var classNames = elem.className.split( /\s+/ );

                        for ( var n = classNames.length; n--; ) {
                            if ( value.test(classNames[n]) ) {
                                classNames.splice(n, 1);
                            }
                        }
                        elem.className = jQuery.trim( classNames.join(" ") );
                    }
                }
            } else {
                removeClass.call(this, value);
            }
            return this;
        }

    })(jQuery.fn.removeClass);

    this.eventWireUp();
}

SideBar.prototype.build = function (url, json, selector) {
    var selfClass = this;
    $.get(url, function (data) {
        var template=Handlebars.compile(data);
        $(selector).html(template(json));
    }).done(function () {
        selfClass.render();
        //selfClass.selector = selector;
    });
    return this;
};

SideBar.prototype.changePosition = function (position) {
    var sidebar = $('#sidebar');
    var toggleButtons = $('.sidebar-toggle');    

    sidebar.removeClass('sidebar-fixed-left sidebar-fixed-right sidebar-stacked').addClass(position).addClass('open');
    if (position == 'sidebar-fixed-left' || position == 'sidebar-fixed-right') {
        $('.sidebar-overlay').addClass('active');
    }
    // Show toggle buttons
    if (position != '') {
        toggleButtons.css('display', 'initial');
        $('body').css('display', 'initial');
    } else {
        // Hide toggle buttons
        toggleButtons.css('display', 'none');
        $('body').css('display', 'table');
    }    
}

SideBar.prototype.changeCoverImage = function(url) {
    if(this.preloadImage(url)) {
    var selfClass = this;
    var sidebarHeader = $('#sidebar .sidebar-header');    
    if (url == '') {
        $('.sidebar-header').removeClass('header-cover').addClass('sidebar-header');        
        sidebarHeader.delay(3000).css('background-image', SideBar.DEFUALT_IMAGE)        
    } else {
        $('.sidebar-header').addClass('sidebar-header');
        sidebarHeader.delay(3000).css('background-image', selfClass.buildBgUrlString(url))
    }
}
    //sidebarHeader.css('background-position', 'center center');
}

SideBar.prototype.changeTheme = function(theme) {
    var sidebar = $('#sidebar');        
    sidebar.removeClass('sidebar-default sidebar-inverse sidebar-colored sidebar-colored-inverse').addClass(theme);
}

SideBar.prototype.changeProfilePic = function(picUrl) {
    if(this.preloadImage(url)) { }
}



//sidebar = new SideBar().build('templates/sidebar.tpl', json, 'body');