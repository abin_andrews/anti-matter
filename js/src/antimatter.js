/*
*	JS Builder Script
*
*/

//Global varaible based on the Backbone.js
antimatter = null;

function Antimatter() {		
	this.constructor.instanceActive++;
}

Antimatter.prototype.uniqueID = function() {
		return 'xxyxxxxy-xyyy-yxxy-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random()*16|0,v=c=='x'?r:r&0x3|0x8;
			return v.toString(16);});
}

Antimatter.prototype.templateLoad = function(url, json, selector) {
	$.get(url, function (data) {
    var template=Handlebars.compile(data);
    $(selector).html(template(json));
    }, 'html');
}

Antimatter.prototype.buildBgUrlString = function(url) {
	return 'url('+url+')';
}

Antimatter.prototype.render = function () {

}

Antimatter.prototype.preloadImages = function (arrayOfImagesUrl) {
	return $(arrayOfImagesUrl).each(function(){
        $('<img/>')[0].src = this;
        // Alternatively you could use:
        // (new Image()).src = this;
    });
}

Antimatter.prototype.preloadImage = function (imageUrl) {
	return $('<img/>')[0].src = imageUrl;      
}

Antimatter.prototype.constructor = Antimatter;
function Antimatter() {
	this.constructor.instanceActive++;    
    this.uuid = this.uniqueID();
}



Antimatter.instanceActive = 0;
//antimatter = new Antimatter();