<html>
<head>

</head>
<body>
<style>

    /* clearfix */
    .grid:after {
        content: '';
        display: block;
        clear: both;
    }

    /* ---- grid-item ---- */

    .grid-sizer,
    .grid-item {
        width: 20%;
    }

    .grid-item {
        float: left;
        border: 1px solid #333;
        border-color: hsla(0, 0%, 0%, 0.5);
    }

    .grid-item--width2 { width:  40%; }
</style>
<div class="grid">
    <div class="grid-sizer"></div>
    <div class="grid-item">
        Lorem ipsum dolor sit amet, Eos primis hendrerit ne.
    </div>
    <div class="grid-item grid-item--width2">
        Sea et diam animal, nemore pericula maluisset vimne.ne eam, ea nisl persecuti voluptatibus vis.
    </div>
    <div class="grid-item">
        Lorem ipsum dolor sit amet, Eos primis hendrerit ne.
    </div>
    <div class="grid-item">
        Lorem ipsum dolor sit amet, Eos primis hendrerit ne.
    </div>
    <div class="grid-item grid-item--width2">
        Sea et diam animal, nemore pericula maluisset vimne.ne eam, ea nisl persecuti voluptatibus vis.
    </div>
    <div class="grid-item">
        Lorem ipsum dolor sit amet, Eos primis hendrerit ne.
    </div>
    <div class="grid-item">
        Lorem ipsum dolor sit amet, Eos primis hendrerit ne.
    </div>
    <div class="grid-item grid-item--width2">
        Sea et diam animal, nemore pericula maluisset vimne.ne eam, ea nisl persecuti voluptatibus vis.
    </div>
    <div class="grid-item">
        Lorem ipsum dolor sit amet, Eos primis hendrerit ne.
    </div>
    <div class="grid-item grid-item--width2">
        Sea et diam animal, nemore pericula maluisset vimne.ne eam, ea nisl persecuti voluptatibus vis.
    </div>
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>
<script>
    var elem = document.querySelector('.grid');
    var msnry = new Masonry( elem, {
        // options
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true
    });

    // element argument can be a selector string
    //   for an individual element
    var msnry = new Masonry( '.grid', {
        // options
    });
</script>
</body>
</html>